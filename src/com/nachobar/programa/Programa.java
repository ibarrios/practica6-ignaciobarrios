package com.nachobar.programa;

/*
 * Clse principal en la que se va a ejecutar el programa
 * @author nachobar
 */

import java.util.Scanner;
import com.nachobar.clases.GestorPersonajes;
import com.nachobar.clases.JefeFinal;
import com.nachobar.clases.Luchador;

public class Programa {


	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);
		String opcion;
		GestorPersonajes personajes = new GestorPersonajes();
		System.out.println("Se crea un Gestor de Personajes para las diferentes opciones..");
		do {
			System.out.println("Introduce una opcion o 0 para salir\n "
					+ "\n 1 .- Crear tres Luchadores y los muestra"
					+ "\n 2 .- Buscar un Luchador"
					+ "\n 3 .- Elimina un luchador y los muestra todos"
					+ "\n 4 .- Crear tres Jefes finales y los muestra "
					+ "\n 5 .- Busca un Jefe Final" 
					+ "\n 6 .- Elimina un Jefe final y los muestra"
					+ "\n 7 .- Lista los Jefes Finales usando el nombre del SuperAtaque"
					+ "\n 8 .- Asigna un Jefe Final a un Luchador como su Nemesis"
					+ "\n 9 .-  Lista los Luchadores por el ataque de su Nemesis"
					+ "\n 10 .- Metodo extra: un luchador ataque a un jefe final"
					+ "\n 11 .- Metodo extra: Listar todos los personajes creados"
					+ "\n");
			opcion = input.nextLine(); 


			switch (opcion) {
			case "1":
				System.out.println("Vas a crear tres luchadores");
				for (int i = 1; i < 4; i++) {
					System.out.println("Introduce los datos del Luchador " + i );
					System.out.println("Nombre");
					String nombreLuchador = input.nextLine();
					System.out.println("Vida");
					int vidaLuchador = Integer.parseInt(input.nextLine());
					System.out.println("Defensa");
					int defensaLuchador = Integer.parseInt(input.nextLine());
					System.out.println("Nombre del arma");
					String nombreArma = input.nextLine();
					System.out.println("Ataque del arma");
					int ataqueArma = Integer.parseInt(input.nextLine());
					personajes.altaLuchador(nombreLuchador, vidaLuchador, defensaLuchador, nombreArma, ataqueArma);
					System.out.println("\n");
				}
				System.out.println("Estos son los tres luchadores que has creado");
				personajes.listarLuchadores();

				break;
			case "2":
				System.out.println("Vas a buscar un luchador, introduce el nombre del luchador que quieres buscar");
				String nombreLuchadorBuscado = input.nextLine();
				Luchador luchadorBuscado = personajes.buscarLuchador(nombreLuchadorBuscado);

				if (luchadorBuscado == null) {
					System.out.println("El luchador buscado no existe");			
				} else {
					System.out.println(luchadorBuscado);
				}
				break;
			case "3":
				System.out.println("Vas a eliminar a un luchador, introduce el nombre del personaje que quieres eliminar");
				String luchadorEliminado = input.nextLine();
				if (personajes.existeLuchador(luchadorEliminado)) {
					personajes.eliminarLuchador(luchadorEliminado);
					System.out.println("Listamos los luchadores despues de haber eliminado uno");
					personajes.listarLuchadores();
				} else {
					System.out.println("El luchador a eliminar no existe");
				}
				break;
			case "4":
				System.out.println("Vas a crear tres jefes finales");
				for (int i = 1; i < 4; i++) {
					System.out.println("Introduce los datos del Jefe final " + i );
					System.out.println("Nombre");
					String nombreJF = input.nextLine();
					System.out.println("Vida");
					int vidaJF = Integer.parseInt(input.nextLine());
					System.out.println("Defensa");
					int defensaJF = Integer.parseInt(input.nextLine());
					System.out.println("Nombre del Super Ataque");
					String nombreSuperAtaque = input.nextLine();
					System.out.println("Valor del super ataque");
					int valorSuperAtaque = Integer.parseInt(input.nextLine());
					personajes.altaJefeFinal(nombreJF, vidaJF, defensaJF, nombreSuperAtaque, valorSuperAtaque);
					System.out.println("\n");
				}		
				break;
			case "5":
				System.out.println("Vas a buscar un luchador, introduce el nombre del luchador que quieres buscar");
				String nombreJefeFinal = input.nextLine();
				JefeFinal jefeFinalBuscado = personajes.buscarJefeFinal(nombreJefeFinal);

				if (jefeFinalBuscado == null) {
					System.out.println("El JefeFinal buscado no existe");			
				} else {
					System.out.println(jefeFinalBuscado);
				}
				break;
			case "6":
				System.out.println("Vas a eliminar a un Jefe Final, introduce el nombre del personaje que quieres eliminar");
				String jefeFinalEliminado = input.nextLine();
				if (personajes.existeJefeFinal(jefeFinalEliminado)) {
					personajes.eliminarJefeFinal(jefeFinalEliminado);
					System.out.println("Listamos los Jefe Final despues de haber eliminado uno");
					personajes.listarJefesFinales();
				} else {
					System.out.println("El Jefe Final a eliminar no existe");
				}
				break;
			case "7":
				System.out.println("Introduce el nombre de un Super Ataque para ver los Jefes Finales que lo tiene");
				String ataqueDeJF = input.nextLine();
				personajes.listarJefeFinalPorAtaque(ataqueDeJF);
				break;
			case "8":
				System.out.println("Asignar un Jefe final como 'Nemesis' a un Luchador\n"
						+ "Introduce el nombre primero del Luchador y luego el del Jefe Final");
				String nombreLuchador = input.nextLine();
				String nombreNemesis = input.nextLine();		
				personajes.asignarNemesis(nombreLuchador, nombreNemesis);
				break;
			case "9":
				System.out.println("Buscar luchadores con un Jefe Final asignado por su Super Ataque, introduce el super ataque ");
				String elSuperAtaque = input.nextLine();
				personajes.listarLuchadoresPorSuperAtaque(elSuperAtaque);
				break;
			case "10":
				System.out.println("Un luchador va a atacar a un Jefe Final y se mostrar� la vida que le queda al jefe");
				System.out.println("Introduce el Luchador");
				String elLuchador = input.nextLine();
				System.out.println("Introduce el Jefe Final");
				String elJefe = input.nextLine();
				personajes.luchadorAtacaJefe(elLuchador, elJefe);
				break;
			case "11":
				personajes.listarPersonajes();
				break;
			case "0":
				System.out.println("ESTAS SALIENDO DEL PROGRAMA");
				break;

			default:
				System.out.println("OPCION INCORRECTA");

				break;
			}


		} while (!opcion.equals("0"));

		input.close();
	}

}
