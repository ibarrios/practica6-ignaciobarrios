package com.nachobar.clases;


/*
 * Clase que guarda los atrinutos de los jefes finales
 * @author nachobar
 */

public class JefeFinal {
	
	/**
	 * Constructor
	 * genera un nuevo Luchador dados todos los atributos
	 * @param nombre es el identificador que tendra el Jefe Final
	 * @param vida que es la vida total del Jefe Final
	 * @param defensa que es la defensa total del Jefe Final
	 * @param nombreSuperAtaque nombre del Super Ataque que tiene el Jefe Final
	 * @param valorSuperAtaque valor del ataque del Super Ataque del Jefe Final
	 */

	private String nombre;
	private int vida;
	private int defensa;
	private String nombreSuperAtaque;
	private int valorSuperAtaque;

	public JefeFinal(String nombre, int vida, int defensa, String nombreSuperAtaque, int valorSuperAtaque) {

		this.nombre = nombre;
		this.vida = vida;
		this.defensa = defensa;
		this.nombreSuperAtaque = nombreSuperAtaque;
		this.valorSuperAtaque = valorSuperAtaque;
	}


	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}

	public String getNombreSuperAtaque() {
		return nombreSuperAtaque;
	}

	public void setNombreSuperAtaque(String nombreSuperAtaque) {
		this.nombreSuperAtaque = nombreSuperAtaque;
	}

	public int getValorSuperAtaque() {
		return valorSuperAtaque;
	}

	public void setValorSuperAtaque(int valorSuperAtaque) {
		this.valorSuperAtaque = valorSuperAtaque;
	}


	@Override
	public String toString() {
		return "JefeFinal [nombre=" + nombre + ", vida=" + vida + ", defensa=" + defensa + ", nombreSuperAtaque="
				+ nombreSuperAtaque + ", valorSuperAtaque=" + valorSuperAtaque + "]";
	}




}
