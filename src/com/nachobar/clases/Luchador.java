package com.nachobar.clases;


/*
 * Clase que guarda los atributos de los Luchadores
 * @author nachobar
 */

public class Luchador {
	
	/**
	 * Constructor
	 * genera un nuevo Luchador dados todos los atributos
	 * @param nombre es el identificador que tendra el Luchador
	 * @param vida que es la vida total del Luchador
	 * @param defensa que es la defensa total del Luchador
	 * @param nombreArma del arma que tiene el Luchador
	 * @param ataqueArma valor del ataque del arma del Luchador
	 */

	private String nombre;
	private int vida;
	private int defensa;
	private String nombreArma;
	private int ataqueArma;
	private JefeFinal nemesis;

	public Luchador(String nombre, int vida, int defensa, String nombreArma, int ataqueArma) {

		this.nombre = nombre;
		this.vida = vida;
		this.defensa = defensa;
		this.nombreArma = nombreArma;
		this.ataqueArma = ataqueArma;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getVida() {
		return vida;
	}
	public void setVida(int vida) {
		this.vida = vida;
	}
	public int getDefensa() {
		return defensa;
	}
	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}
	public String getNombreArma() {
		return nombreArma;
	}
	public void setNombreArma(String nombreArma) {
		this.nombreArma = nombreArma;
	}
	public int getAtaqueArma() {
		return ataqueArma;
	}
	public void setAtaqueArma(int ataqueArma) {
		this.ataqueArma = ataqueArma;
	}

	public JefeFinal getNemesis() {
		return nemesis;
	}

	public void setNemesis(JefeFinal jefeFinal) {
		this.nemesis = jefeFinal;
	}

	@Override
	public String toString() {
		return "Luchador [nombre=" + nombre + ", vida=" + vida + ", defensa=" + defensa + ", nombreArma=" + nombreArma
				+ ", ataqueArma=" + ataqueArma + "]";
	}




}
