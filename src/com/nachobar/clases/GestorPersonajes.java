package com.nachobar.clases;

/*
 * Clse principal que guarda los ArrayList de los luchadores y jefes finales as� como los metodos del programa
 * @author nachobar
 */

import java.util.ArrayList;
import java.util.Iterator;

public class GestorPersonajes {
	
	
	/**
	 * Constructor
	 * genera un nuevo Luchador dados todos los atributos
	 * @param listaLuchadores arraylist que almacena objetos Luchador
	 * @param listaJefesFinales arraylist que almacena objetos JefeFinal

	 */

	private ArrayList<Luchador> listaLuchadores;
	private ArrayList<JefeFinal> listaJefesFinales;


	public GestorPersonajes() {

		this.listaLuchadores =  new ArrayList<Luchador>();
		this.listaJefesFinales = new ArrayList<JefeFinal>();

	}
	
	/* 
	 * da de alta un objeto luchador usando el contructor de la clase luchador y a�adiendolo al arrayList listaLuchadores
	 * Si el luchador ya existe lo dice
	 * @param nombre el nombre del Luchador
	 *@param vida el valor de la vida del Luchador
	 *@param defensa el valor de la defensa del Luchador
	 *@param nombreArma el nombre del arma del Luchador
	 *@param ataqueArma el valor del ataque del Luchador
	 *	 
	 */

	public void altaLuchador(String nombre, int vida, int defensa, String nombreArma, int ataqueArma) {
		if ((!existeLuchador(nombre))) {
			Luchador nuevoLuchador = new Luchador(nombre,vida,defensa,nombreArma,ataqueArma);

			listaLuchadores.add(nuevoLuchador);
		} else {
			System.out.println("El Luchador ya existe");
		}
	}
	
	/* 
	 * Verifica si un Luchador existe dentro del arrayList con un nombre dado
	 * @param nombre de un luchador
	 * @return booleando si el resultado es que el luchador existe on o como True o False
	 *	 
	 */

	public boolean existeLuchador(String nombre) {
		for (Luchador luchador : listaLuchadores) {
			if (luchador != null && luchador.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}
	
	/* 
	 * lista todos los objetos luchador dentro del arraylist de luchadores con todos sus paramentros
	 *	 
	 */

	public void listarLuchadores() {

		for (int i = 0; i < listaLuchadores.size(); i++) {
			System.out.println(listaLuchadores.get(i));
		}

	}

	
	/* 
	 * busca entre los Luchadores dentro de arrayList de luchadores con un nombre dado
	 * @param nombre el nombre del Luchador que se quiere buscar
	 * @return devuelve un objeto luchador
	 *	 
	 */
	
	public Luchador buscarLuchador(String nombre) {
		for (Luchador luchador : listaLuchadores) {
			if (luchador != null && luchador.getNombre().equals(nombre)) {
				return luchador;
			}
		}
		return null;
	}

	/* 
	 * elimina un luchador dentro del arraylist de luchadores leyendo el nombre del luchador a borrar
	 * @param nombre el nombre del Luchador que se quiere eliminar
	 *	 
	 */

	public void eliminarLuchador(String nombre) {
		Iterator<Luchador> iteradorLuchador = listaLuchadores.iterator();

		while (iteradorLuchador.hasNext()) {
			Luchador luchador = iteradorLuchador.next();
			if (luchador.getNombre().equals(nombre)) {
				iteradorLuchador.remove();
			}
		}
	}

	/* 
	 * da de alta un objeto JefeFinal usando el contructor de la clase Jefe Final y a�adiendolo al arrayList listaJefesFinales
	 * Si el JefeFinal ya existe lo dice
	 * @param nombre el nombre del JefeFinalJefeFinal
	 *@param vida el valor de la vida del JefeFinal
	 *@param defensa el valor de la defensa del JefeFinal
	 *@param nombreSuperAtaque el nombre del Super Ataque del JefeFinal
	 *@param valorSuperAtaque el valor del Super Ataque del JefeFinal
	 *	 
	 */


	public void altaJefeFinal(String nombre, int vida, int defensa, String nombreSuperAtaque, int valorSuperAtaque) {
		if ((!existeLuchador(nombre))) {
			JefeFinal nuevoJefeFinal = new JefeFinal(nombre,vida,defensa,nombreSuperAtaque,valorSuperAtaque);

			listaJefesFinales.add(nuevoJefeFinal);
		} else {
			System.out.println("El Jefe Final ya existe");
		}
	}
	
	/* 
	 * Verifica si un Jefe Final existe dentro del arrayList con un nombre dado
	 * @param nombre de un Jefe Final
	 * @return booleando si el resultado es que el Jefe Final existe o o como True o False
	 *	 
	 */

	
	public boolean existeJefeFinal(String nombre) {
		for (JefeFinal jefefinal : listaJefesFinales) {
			if (jefefinal != null && jefefinal.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;
	}


	/* 
	 * elimina un Jefe Final dentro del arraylist de jefes finales leyendo el nombre del Jefe Final a borrar
	 * @param nombre el nombre del Jefe Final que se quiere eliminar
	 *	 
	 */
	
	public void eliminarJefeFinal(String nombre) {
		Iterator<JefeFinal> iteradorJefeFinal = listaJefesFinales.iterator();

		while (iteradorJefeFinal.hasNext()) {
			JefeFinal jefeFinal = iteradorJefeFinal.next();
			if (jefeFinal.getNombre().equals(nombre)) {
				iteradorJefeFinal.remove();
			}
		}
	}
	
	/* 
	 * busca entre los Jefe Final dentro de arrayList de Jefe Final con un nombre dado
	 * @param nombre el nombre del Jefe Final que se quiere buscar
	 * @return devuelve un objeto Jefe Final
	 *	 
	 */

	public JefeFinal buscarJefeFinal(String nombre) {
		for (JefeFinal jefeFinal : listaJefesFinales) {
			if (jefeFinal != null && jefeFinal.getNombre().equals(nombre)) {
				return jefeFinal;
			}
		}
		return null;
	}
	
	/* 
	 * lista todos los jefes finales dentro del arraylist que tengan el mismo tipo de un ataque leido por teclado
	 * @param nombreAtaque el nombre del ataque que se usar� para buscar
	 *	 
	 */

	public void listarJefeFinalPorAtaque(String nombreAtaque) {
		for (JefeFinal jefeFinal : listaJefesFinales) {
			if ( jefeFinal.getNombreSuperAtaque().equals(nombreAtaque)) {
				System.out.println(jefeFinal);
				
			}

		}
	}

	/* 
	 * lista todos los liuchadores dentro del arraylist que tengan un Jefe Final como Nemesis asignado y que ese Jefe Final tenga el mismo tipo de un ataque leido por teclado
	 * @param nombreAtaque el nombre del ataque que se usar� para buscar
	 *	 
	 */

	public void listarLuchadoresPorSuperAtaque(String nombre) {
		for (Luchador luchador : listaLuchadores) {
			if (luchador.getNemesis() != null && luchador.getNemesis().getNombreSuperAtaque().equals(nombre)) {
				System.out.println(luchador);
			}
		}
	}

	/* 
	 * asigna al parametro Nemesis de un luchador leido por teclado un Jefe Final que se leer� por teclado
	 * @param nombreLuchador el nombre del luchador al que se le va a asignar un Jefe Final
	 * @param nombreNemesis el Jefe Final que se asigna
	 *	 
	 */

	public void asignarNemesis(String nombreLuchador, String nombreNemesis) {
		if (buscarLuchador(nombreLuchador) != null && buscarJefeFinal(nombreNemesis) != null) {
			JefeFinal jefeFinal =  buscarJefeFinal(nombreNemesis);
			Luchador luchador =buscarLuchador(nombreLuchador);
			luchador.setNemesis(jefeFinal); 
		}
	}

	/* 
	 * Se listan todos los jefes finales dentro dle arrayList
	 * 
	 *	 
	 */
	
	public void listarJefesFinales() {

		for (int i = 0; i < listaJefesFinales.size(); i++) {
			System.out.println(listaJefesFinales.get(i));
		}
	}

	/* 
	 * 
	 * se listan todos los personajes creados en el juego
	 *	 
	 */
	
	public void listarPersonajes() {

		System.out.println(listaJefesFinales);
		System.out.println(listaLuchadores);

	}

	/* 
	 * Un luchador leido por teclado ataca a un jefe leido por teclado
	 * el metodo mostrar� el nombre de ambos y el valor final de la vida que le queda al jefe final
	 * @param nombreLuchador el nombre del Luchador que lanza el ataque
	 * @param nombreJefe el nombre del jefe que recibe el ataque
	 *	 
	 */

	public void luchadorAtacaJefe(String nombreLuchador,String nombreJefe) {

		if (existeLuchador(nombreLuchador)) {
			if (existeJefeFinal(nombreJefe)) {
				Luchador elLuchador = buscarLuchador(nombreLuchador);
				JefeFinal elJefe = buscarJefeFinal(nombreJefe);

				System.out.println("El luchador " + elLuchador.getNombre() + " ha atacado al Jefe " + elJefe.getNombre() + " y le ha dejado con vida "
						+ "" + (elJefe.getVida()-elLuchador.getAtaqueArma()) );				
			} else {
				System.out.println("El jefe no existe");
			}
		} else {
			System.out.println("El luchador no existe");
		}


	}

}


